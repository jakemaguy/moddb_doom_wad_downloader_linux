FROM python:3-alpine
RUN apk add --no-cache curl

RUN mkdir build
COPY ./get_wad.py ./
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT [ "python3", "/get_wad.py" ]
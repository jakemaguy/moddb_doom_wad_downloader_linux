#!/usr/bin/env bash

WAD_LINK=$1
WAD_FILENAME=$2

docker build . -t wad_downloader

docker run --rm  -v ~/.config/gzdoom:/build --name wad_downloader wad_downloader ${WAD_LINK} ${WAD_FILENAME}

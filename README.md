# Purpose

This tool will download a moddb doom wad, copy to the gzdoom wads directory, and unzip/delete temporary zip files for you.  

# Example - Installing Sunlust

1. Go to https://www.moddb.com/mods/sunlust/downloads/sunlust1
2. Copy and paste the filename in the mod description.  For this example it's: sunlust.zip
3. Right clight the download now button, and copy link source
4. git clone https://gitlab.com/jakemaguy/moddb_doom_wad_downloader_linux.git
5. cd moddb_doom_wad_downloader_linux
6. ./run.sh <mod_download_url> <mod_filename>
7. Your wads will be put in ~/.config/gzdoom/

for sunlust:

`gzdoom  -file ~/.config/gzdoom/sunlust.wad`

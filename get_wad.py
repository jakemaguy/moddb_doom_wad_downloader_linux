import os
import re
import sys
import glob
import shutil
import zipfile
import requests

DOWNLOAD_LINK = sys.argv[1]
WAD_FILENAME = sys.argv[2]
WAD_FILENAME_NO_EXT = WAD_FILENAME.split('.')[0]

r = requests.get(DOWNLOAD_LINK, headers = {'Accept': 'application/json'}, allow_redirects=True)
data = r.text

download_link = re.search(r'<a href="(\S*)">download {}'.format(WAD_FILENAME), data).group(1)
r = requests.get("https://www.moddb.com" + download_link)
# write the file
open(WAD_FILENAME, 'wb').write(r.content)

with zipfile.ZipFile(WAD_FILENAME, 'r') as zip_ref:
    zip_ref.extractall()

if os.path.isdir(WAD_FILENAME_NO_EXT + '/'):
    for file in glob.glob('/' + WAD_FILENAME_NO_EXT + '/*.wad'):
        shutil.copy(file, '/build')
else:
    for file in glob.glob('/*.wad'):
        shutil.copy(file, '/build')





